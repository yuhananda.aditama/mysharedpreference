package id.bootcamp.mysharedpreference.data.local.room

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import id.bootcamp.mysharedpreference.data.local.entity.ExampleEntity

@Database(entities = [ExampleEntity::class], version = 1, exportSchema = false)
abstract class ExampleDatabase : RoomDatabase() {

    abstract fun exampleDao(): ExampleDao

    companion object {
        @Volatile
        private var instance: ExampleDatabase? = null
        fun getInstance(context: Context): ExampleDatabase =
            instance ?: synchronized(this) {
                instance ?: Room.databaseBuilder(
                    context.applicationContext,
                    ExampleDatabase::class.java, "Example.db"
                ).createFromAsset("database/example.db").build()
            }
    }
}