package id.bootcamp.mysharedpreference.data.remote.response

import kotlinx.parcelize.Parcelize
import android.os.Parcelable
import com.google.gson.annotations.SerializedName

@Parcelize
data class ReqresSingleUserResponse(

	@field:SerializedName("data")
	val data: Data? = null,

	@field:SerializedName("support")
	val support: Support? = null
) : Parcelable

@Parcelize
data class Data(

	@field:SerializedName("last_name")
	val lastName: String? = null,

	@field:SerializedName("id")
	val id: Int? = null,

	@field:SerializedName("avatar")
	val avatar: String? = null,

	@field:SerializedName("first_name")
	val firstName: String? = null,

	@field:SerializedName("email")
	val email: String? = null
) : Parcelable
