package id.bootcamp.mysharedpreference.data

import id.bootcamp.mysharedpreference.data.local.ExampleSharedPreference
import id.bootcamp.mysharedpreference.data.local.UserSharedPreference
import id.bootcamp.mysharedpreference.data.local.room.ExampleDao
import id.bootcamp.mysharedpreference.data.remote.retrofit.RegresApiService
import id.bootcamp.mysharedpreference.ui.data.ExampleData
import id.bootcamp.mysharedpreference.ui.data.ExamplePage
import id.bootcamp.mysharedpreference.ui.data.UserData

class UserRepository private constructor(
    private val userSharedPreference: UserSharedPreference
) {

    suspend fun getUserData() : UserData{
        val data = userSharedPreference.getUserData()
        return data
    }
    suspend fun saveUserData(userData: UserData){
        userSharedPreference.saveUserData(userData)
    }
    companion object {
        @Volatile
        private var instance: UserRepository? = null
        fun getInstance(
           userSharedPreference: UserSharedPreference,
        ): UserRepository =
            instance ?: synchronized(this) {
                instance ?: UserRepository(userSharedPreference)
            }.also { instance = it }
    }
}