package id.bootcamp.mysharedpreference.data

import id.bootcamp.mysharedpreference.data.local.ExampleSharedPreference
import id.bootcamp.mysharedpreference.data.local.room.ExampleDao
import id.bootcamp.mysharedpreference.data.remote.retrofit.RegresApiService
import id.bootcamp.mysharedpreference.ui.data.ExampleData
import id.bootcamp.mysharedpreference.ui.data.ExamplePage

class ExampleRepository private constructor(
    private val regresApiService: RegresApiService,
    private val exampleDao: ExampleDao,
    private val exampleSharedPreference: ExampleSharedPreference
) {
    suspend fun getExampleListFromApi(page: Int): ExamplePage<List<ExampleData>> {
        //Get From API
        val exampleList = ArrayList<ExampleData>()
        val examplePage = ExamplePage<List<ExampleData>>(
            1,
            1,
            exampleList
        )

        try {
            val responseApi = regresApiService.getUser(page)
            //mapping dari Response ke ExampleData
            responseApi.data?.map {
                exampleList.add(
                    ExampleData(
                        it?.firstName ?: "",
                        it?.lastName ?: "",
                        it?.avatar ?: ""
                    )
                )
            }

            examplePage.page = responseApi.page ?: 1
            examplePage.totalPage = responseApi.totalPages ?: 1
            examplePage.data = exampleList
        } catch (e: Exception) {
            e.printStackTrace()
        }

        return examplePage
    }

    companion object {
        @Volatile
        private var instance: ExampleRepository? = null
        fun getInstance(
            regresApiService: RegresApiService,
            exampleDao: ExampleDao,
            exampleSharedPreference: ExampleSharedPreference
        ): ExampleRepository =
            instance ?: synchronized(this) {
                instance ?: ExampleRepository(regresApiService, exampleDao,exampleSharedPreference)
            }.also { instance = it }
    }
}