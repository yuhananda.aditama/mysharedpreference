package id.bootcamp.mysharedpreference.data.local

import android.content.Context
import id.bootcamp.mysharedpreference.ui.data.UserData

class UserSharedPreference(context: Context) {
    companion object {
        private const val PREFS_NAME = "user_pref"

        private const val NAME_KEY = "name"
        private const val EMAIL_KEY = "email"
        private const val PHONE_KEY = "phone"
        private const val AGE_KEY = "age"
        private const val MU_KEY = "mu"

    }

    //Membuat objek untuk koneksi ke sharedpreference yang namanya PREFS_NAME
    private val preference = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE)

    //Simpan data di shared preference
    fun saveUserData(userData: UserData) {
        val editor = preference.edit()
        editor.putString(NAME_KEY, userData.name)
        editor.putString(EMAIL_KEY, userData.email)
        editor.putString(PHONE_KEY, userData.phone)
        editor.putInt(AGE_KEY, userData.age)
        editor.putBoolean(MU_KEY, userData.likeMu)
        editor.apply()
    }

    //Mengambil data dari shared preference
    fun getUserData(): UserData {
        val userData = UserData()
        userData.name = preference.getString(NAME_KEY, "") ?: ""
        userData.email = preference.getString(EMAIL_KEY, "") ?: ""
        userData.phone = preference.getString(PHONE_KEY, "") ?: ""
        userData.age = preference.getInt(AGE_KEY, 0)
        userData.likeMu = preference.getBoolean(MU_KEY, false)
        return userData
    }
}