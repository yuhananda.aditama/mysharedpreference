package id.bootcamp.mysharedpreference.data.local.room

import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import androidx.room.Update
import id.bootcamp.mysharedpreference.data.local.entity.ExampleEntity

@Dao
interface ExampleDao {

    @Query("SELECT * FROM example")
    suspend fun getExamples(): List<ExampleEntity>

    @Query("SELECT * FROM example WHERE id = :id")
    suspend fun getExampleById(id: Int): ExampleEntity

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun insertPopulateData(example: List<ExampleEntity>)

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    suspend fun insertExamples(example: List<ExampleEntity>)

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    suspend fun insertExample(example: ExampleEntity)

    @Update
    suspend fun updateExample(example: ExampleEntity)

    @Delete
    suspend fun deleteExample(example: ExampleEntity)
}