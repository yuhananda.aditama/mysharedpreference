package id.bootcamp.mysharedpreference.ui.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import id.bootcamp.mysharedpreference.data.UserRepository
import id.bootcamp.mysharedpreference.ui.data.UserData
import kotlinx.coroutines.launch

//1. Tambahkan konstruktor & properti userRepository
//2. Buat class ini extends / child dari Class ViewModel
//3. Daftarkan class ini di UserViewModelFactory
class AddEditUserViewModel(val userRepository: UserRepository) : ViewModel() {

    //Buatlah fungsi untuk save user
    //  Note : jangan lupa gunakan viewModelScope.launch
    fun saveUser(userData: UserData) = viewModelScope.launch {
        //  Buatlah logic didalam fungsi untuk save user data ke repository
        userRepository.saveUserData(userData)
    }


}