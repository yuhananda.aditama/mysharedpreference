package id.bootcamp.mysharedpreference.ui

import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import com.bumptech.glide.Glide
import id.bootcamp.mysharedpreference.databinding.ActivityMainDetailBinding
import id.bootcamp.mysharedpreference.ui.data.ExampleData

class MainDetailActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainDetailBinding
    private var exampleData: ExampleData? = null

    companion object {
        const val EXTRA_EXAMPLE_DATA = "EXTRA_EXAMPLE_DATA"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainDetailBinding.inflate(layoutInflater)
        setContentView(binding.root)

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU) {
            exampleData = intent.getParcelableExtra(EXTRA_EXAMPLE_DATA, ExampleData::class.java)
        } else {
            exampleData = intent.getParcelableExtra(EXTRA_EXAMPLE_DATA)
        }

        supportActionBar?.title = exampleData?.firstName
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        Glide.with(this).load(exampleData?.avatar).into(binding.civAvatar)
        binding.tvName.text = exampleData?.firstName + " " + exampleData?.lastName
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when(item.itemId){
            android.R.id.home -> finish()
        }
        return super.onOptionsItemSelected(item)
    }
}