package id.bootcamp.mysharedpreference.ui.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import id.bootcamp.mysharedpreference.data.ExampleRepository
import id.bootcamp.mysharedpreference.ui.data.ExampleData
import kotlinx.coroutines.launch

class ExampleViewModel(private val exampleRepository: ExampleRepository) : ViewModel() {
    val exampleDataList: MutableLiveData<List<ExampleData>> = MutableLiveData()
    var page = 1
    var totalPage = 1
    fun getExampleDataList() = viewModelScope.launch {
        val data = exampleRepository.getExampleListFromApi(page)
        page = data.page
        totalPage = data.totalPage

        exampleDataList.postValue(data.data as List<ExampleData>)
    }
}