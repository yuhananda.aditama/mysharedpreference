package id.bootcamp.mysharedpreference.ui.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import de.hdodenhof.circleimageview.CircleImageView
import id.bootcamp.mysharedpreference.R
import id.bootcamp.mysharedpreference.ui.data.ExampleData

class ExampleAdapter(val exampleDataList: ArrayList<ExampleData>) :
    RecyclerView.Adapter<ExampleAdapter.ViewHolder>() {

    private var onExampleItemListener: OnExampleItemListener? = null

    fun setOnExampleItemListener(onExampleItemListener: OnExampleItemListener) {
        this.onExampleItemListener = onExampleItemListener
    }

    interface OnExampleItemListener {
        fun onItemClick(exampleData: ExampleData)
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val ivAvatar = itemView.findViewById<CircleImageView>(R.id.ivAvatar)
        val tvName = itemView.findViewById<TextView>(R.id.tvName)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ExampleAdapter.ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_example, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ExampleAdapter.ViewHolder, position: Int) {
        val data = exampleDataList[position]
        Glide.with(holder.itemView.context).load(data.avatar).into(holder.ivAvatar)
        holder.tvName.text = "${data.firstName} ${data.lastName}"

        if (onExampleItemListener != null) {
            holder.itemView.setOnClickListener {
                onExampleItemListener?.onItemClick(data)
            }
        }
    }

    override fun getItemCount(): Int {
        return exampleDataList.size
    }

    fun addData(dataList: List<ExampleData>) {
//        val indexBefore = exampleDataList.lastIndex
        exampleDataList.addAll(dataList)
//        val indexAfter = exampleDataList.lastIndex
        notifyDataSetChanged()
//        notifyItemRangeInserted(indexBefore,indexAfter)
    }
}