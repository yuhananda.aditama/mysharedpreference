package id.bootcamp.mysharedpreference.ui.data

data class ExamplePage<T>(
    //T adalah generic class, yang mana classnya akan ditentukan ketika data ini dibuat
    var page: Int,
    var totalPage: Int,
    var data: T
)