package id.bootcamp.mysharedpreference.ui.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import id.bootcamp.mysharedpreference.data.UserRepository
import id.bootcamp.mysharedpreference.ui.data.UserData
import kotlinx.coroutines.launch

class UserViewModel(val userRepository: UserRepository) : ViewModel() {

    val userLiveData = MutableLiveData<UserData>()

    fun getUserData() = viewModelScope.launch {
        val data = userRepository.getUserData()
        userLiveData.postValue(data)
    }

}