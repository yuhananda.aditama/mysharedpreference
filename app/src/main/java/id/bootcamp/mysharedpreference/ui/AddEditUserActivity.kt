package id.bootcamp.mysharedpreference.ui

import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.lifecycle.ViewModelProvider
import id.bootcamp.mysharedpreference.R
import id.bootcamp.mysharedpreference.databinding.ActivityAddEditUserBinding
import id.bootcamp.mysharedpreference.ui.data.UserData
import id.bootcamp.mysharedpreference.ui.viewmodel.AddEditUserViewModel
import id.bootcamp.mysharedpreference.ui.viewmodel.UserViewModelFactory

class AddEditUserActivity : AppCompatActivity() {
    //Deklarasi lateinit binding + viewmodel
    private lateinit var binding : ActivityAddEditUserBinding
    private lateinit var viewModel : AddEditUserViewModel
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        //Inisialisasi dan setContentview dengan binding
        binding = ActivityAddEditUserBinding.inflate(layoutInflater)
        setContentView(binding.root)

        //Menerima data dari activity sebelumnya
        var user : UserData? = null
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU) {
            user = intent.getParcelableExtra("data",UserData::class.java)
        } else{
            user = intent.getParcelableExtra("data")
        }
        //Set view berdasarkan data yang dikirimkan
        binding.edtName.setText(user?.name)
        binding.edtEmail.setText(user?.email)
        binding.edtPhone.setText(user?.phone)
        binding.edtAge.setText(user?.age.toString())
        if (user?.likeMu == true){
            binding.rbYes.isChecked = true
        } else if (user?.likeMu == false){
            binding.rbNo.isChecked = true
        }


        //Inisiaisasi viewmodel
        viewModel = ViewModelProvider(this,UserViewModelFactory.getInstance(this))
            .get(AddEditUserViewModel::class.java)

        //Buat aksi ketika button simpan di klik
        binding.btnSave.setOnClickListener {
            //validasi inputan
            //Deklarasi variabel hasilName lalu dilanjutkan
            //perintah mengambil text didalama view edtName lalu ditampung ke variabel hasilName
            val hasilName = binding.edtName.text.toString().trim()
            val hasilEmail = binding.edtEmail.text.toString().trim()
            val hasilPhone = binding.edtPhone.text.toString().trim()
            val hasilAge = binding.edtAge.text.toString().trim()
            //check id radio button, kalau radio button tidak dipilih hasilnya -1
            val hasilMu = binding.rgLoveMu.checkedRadioButtonId

            //Asumsi, form kita sudah valid
            var isFormValid = true

            if (hasilName.isEmpty()){
                //asumsi salah, ternyata formnya tidak valid
                isFormValid = false
                //perintah memberikan error di edtName
                binding.edtName.error = "Nama tidak boleh kosong"
            }

            if (hasilEmail.isEmpty()){
                //asumsi salah, ternyata formnya tidak valid
                isFormValid = false
                binding.edtEmail.error = "Email tidak boleh kosong"
            }

            if (hasilPhone.isEmpty()){
                //asumsi salah, ternyata formnya tidak valid
                isFormValid = false
                binding.edtPhone.error = "Phone tidak boleh kosong"
            }

            if (hasilAge.isEmpty()){
                //asumsi salah, ternyata formnya tidak valid
                isFormValid = false
                binding.edtAge.error = "Age tidak boleh kosong"
            }

            if (hasilMu == -1){
                //asumsi salah, ternyata formnya tidak valid
                isFormValid = false
                binding.edtName.error = "Anda harus suka MU"
            }


            if (isFormValid == true){
                //buat objek userData
                val userData = UserData()
                userData.name = hasilName
                userData.email = hasilEmail
                userData.phone = hasilPhone
                userData.age = hasilAge.toInt()
                //Jika yg dipilih rb yes maka true sisanya false
                val realHasilMu : Boolean = when(hasilMu){
                    R.id.rb_yes -> true
                    else -> false
                }
                userData.likeMu = realHasilMu
                //kirimkan data yang mau disimpan ke fungsi viewmodel.saveUser
                viewModel.saveUser(userData)
                //tutup halaman ini (menunggunakan fungsi finish())
                finish()
            }

        }


    }
}
