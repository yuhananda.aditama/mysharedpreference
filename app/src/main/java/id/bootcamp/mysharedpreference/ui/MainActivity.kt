package id.bootcamp.mysharedpreference.ui

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.RecyclerView.OnScrollListener
import id.bootcamp.mysharedpreference.R
import id.bootcamp.mysharedpreference.databinding.ActivityMainBinding
import id.bootcamp.mysharedpreference.ui.adapter.ExampleAdapter
import id.bootcamp.mysharedpreference.ui.data.ExampleData
import id.bootcamp.mysharedpreference.ui.viewmodel.ExampleViewModel
import id.bootcamp.mysharedpreference.ui.viewmodel.ExampleViewModelFactory

class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding
    private lateinit var viewModel: ExampleViewModel
    private lateinit var mAdapter: ExampleAdapter
    private var isLoading = true

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        //Inisiasi Viewmodel
        viewModel = ViewModelProvider(
            this,
            ExampleViewModelFactory.getInstance(this)
        ).get(ExampleViewModel::class.java)

        //Setup Action Bar
        supportActionBar?.title = "Example Data"

        //Setup Recyclerview
        mAdapter = ExampleAdapter(ArrayList())
        binding.rvExample.layoutManager = LinearLayoutManager(this)
        binding.rvExample.adapter = mAdapter

        //Aksi ketika scroll kebawah
        binding.rvExample.addOnScrollListener(object : OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)

                val llManager = recyclerView.layoutManager as LinearLayoutManager
                if (!isLoading) {
                    if (llManager.findLastVisibleItemPosition() == mAdapter.exampleDataList.lastIndex) {
                        if (viewModel.totalPage > viewModel.page) {
                            viewModel.page++
                            binding.pgBar.visibility = View.VISIBLE
                            isLoading = true

                            viewModel.getExampleDataList()
                        }
                    }
                }
            }
        })

        //Observer menunggu data dari server
        binding.pgBar.visibility = View.VISIBLE
        isLoading = true
        viewModel.exampleDataList.observe(this) { dataList ->
            if (dataList != null) {
                //Update List
                mAdapter.addData(dataList)
                viewModel.exampleDataList.postValue(null)

                binding.pgBar.visibility = View.GONE
                isLoading = false
            }
        }
        viewModel.getExampleDataList()

        mAdapter.setOnExampleItemListener(object : ExampleAdapter.OnExampleItemListener{
            override fun onItemClick(exampleData: ExampleData) {
                val intent = Intent(this@MainActivity,MainDetailActivity::class.java)
                intent.putExtra(MainDetailActivity.EXTRA_EXAMPLE_DATA,exampleData)
                startActivity(intent)
            }

        })
    }

    //Set Menu
    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.main_menu, menu)
        return super.onCreateOptionsMenu(menu)
    }

    //Aksi di Menu
    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.menuData -> {
                val intent = Intent(this, CrudRoomActivity::class.java)
                startActivity(intent)
            }
        }
        return super.onOptionsItemSelected(item)
    }
}