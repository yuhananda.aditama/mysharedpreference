package id.bootcamp.mysharedpreference.ui.data

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class UserData(
    var name: String = "",
    var email: String = "",
    var phone: String = "",
    var age: Int = 0,
    var likeMu: Boolean = false
) : Parcelable
