package id.bootcamp.mysharedpreference.ui.viewmodel

import android.content.Context
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import id.bootcamp.mysharedpreference.data.ExampleRepository
import id.bootcamp.mysharedpreference.di.Injection

class ExampleViewModelFactory private constructor(private val exampleRepository: ExampleRepository) :
    ViewModelProvider.NewInstanceFactory() {

    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(ExampleViewModel::class.java)) {
            return ExampleViewModel(exampleRepository) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class: " + modelClass.name)
    }

    companion object {
        @Volatile
        private var instance: ExampleViewModelFactory? = null
        fun getInstance(context: Context): ExampleViewModelFactory =
            instance ?: synchronized(this) {
                instance ?: ExampleViewModelFactory(Injection.provideExampleRepository(context))
            }.also { instance = it }
    }
}