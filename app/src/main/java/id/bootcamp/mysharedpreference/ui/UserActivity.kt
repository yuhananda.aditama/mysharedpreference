package id.bootcamp.mysharedpreference.ui

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.lifecycle.ViewModelProvider
import id.bootcamp.mysharedpreference.R
import id.bootcamp.mysharedpreference.databinding.ActivityUserBinding
import id.bootcamp.mysharedpreference.ui.data.UserData
import id.bootcamp.mysharedpreference.ui.viewmodel.UserViewModel
import id.bootcamp.mysharedpreference.ui.viewmodel.UserViewModelFactory

class UserActivity : AppCompatActivity() {
    private lateinit var binding: ActivityUserBinding
    private lateinit var viewModel: UserViewModel
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityUserBinding.inflate(layoutInflater)
        setContentView(binding.root)

        viewModel = ViewModelProvider(
            this,
            UserViewModelFactory.getInstance(this)
        ).get(UserViewModel::class.java)

        setupDataFromSharedPreference()

        //Tambahkan aksi ketika button Simpan di klik
        binding.btnSave.setOnClickListener {
            //Tambahkan logic untuk pindah ke AddEditUserActivity

            //Ambil data sekarang dari livedata
            val data = viewModel.userLiveData.value ?: UserData()

            val intent = Intent(this,AddEditUserActivity::class.java)
            //put extra
            intent.putExtra("data",data)
            startActivity(intent)
        }
    }

    private fun setupDataFromSharedPreference() {
        //Code untuk mengambil dan mengolah dari sharedpreference
        viewModel.userLiveData.observe(this){
            //ketriger jika ada perubahan di userLiveDatanya
            if (it != null){
                updateUI(it)
            }
        }

        viewModel.getUserData()
    }

    private fun updateUI(userData: UserData) {
        if (userData.name.isNotEmpty()){
            binding.tvName.text = userData.name
        } else{
            binding.tvName.text = "Tidak ada"
        }

        if (userData.email.isNotEmpty()){
            binding.tvEmail.text = userData.email
        } else{
            binding.tvEmail.text = "Tidak ada"
        }

        if (userData.phone.isNotEmpty()){
            binding.tvPhone.text = userData.phone
        } else{
            binding.tvPhone.text = "Tidak ada"
        }

        if (userData.age != 0){
            binding.tvAge.text = "${userData.age} tahun"
        } else{
            binding.tvAge.text = "Tidak ada"
        }

        if (userData.likeMu == true){
            binding.tvIsLoveMu.text = "Suka MU"
        } else{
            binding.tvIsLoveMu.text = "Tidak Suka MU"
        }
    }

    override fun onResume() {
        super.onResume()
        viewModel.getUserData()
    }
}