package id.bootcamp.mysharedpreference.di

import android.content.Context
import id.bootcamp.mysharedpreference.data.ExampleRepository
import id.bootcamp.mysharedpreference.data.UserRepository
import id.bootcamp.mysharedpreference.data.local.ExampleSharedPreference
import id.bootcamp.mysharedpreference.data.local.UserSharedPreference
import id.bootcamp.mysharedpreference.data.local.room.ExampleDatabase
import id.bootcamp.mysharedpreference.data.remote.retrofit.ApiConfig

object Injection {

    fun provideUserRepository(context: Context) : UserRepository{
        val userSharedPreference = UserSharedPreference(context)
        return UserRepository.getInstance(userSharedPreference)
    }
    fun provideExampleRepository(context: Context):ExampleRepository{
        val exampleApiService = ApiConfig.getRegresApiService()
        val exampleDatabase = ExampleDatabase.getInstance(context)
        val exampleDao = exampleDatabase.exampleDao()
        val exampleSharedPreference = ExampleSharedPreference(context)
        return ExampleRepository.getInstance(exampleApiService,exampleDao,exampleSharedPreference)
    }
}